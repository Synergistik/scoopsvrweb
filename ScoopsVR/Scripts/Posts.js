﻿$(document).ready(function () {
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        width: 'auto', // overcomes width:'auto' and maxWidth bug
        maxWidth: 800,
        height: 'auto',
        fluid: true, //new option
        resizable: false
    });

})


$(".panel-footer").on("click", function (e) {
    e.preventDefault();

    var targetPost = $(this).children("#ID").val();
    $.ajax({
        type: "POST",
        url: window.location.href + "GetPostByID",
        data: {id: targetPost + ''},
        success: function (data) {
            showDialog(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });

});


function showDialog(postData) {
    $("#dialog").children("p").html(postData.content);
    $("#dialog p img").remove();    
    $("#dialog").dialog("option", "title", postData.title);
    $("#dialog").dialog("open");

}

// catch dialog if opened within a viewport smaller than the dialog width
$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
    fluidDialog();
});

// on window resize run function
$(window).resize(function () {
    fluidDialog();
});


function fluidDialog() {
    var $visible = $(".ui-dialog:visible");
    // each open dialog
    $visible.each(function () {
        var $this = $(this);
        var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
        // if fluid option == true
        if (dialog.options.fluid) {
            var wWidth = $(window).width();
            // check window width against dialog width
            if (wWidth < (parseInt(dialog.options.maxWidth) + 50)) {
                // keep dialog from filling entire screen
                $this.css("max-width", "90%");
            } else {
                // fix maxWidth bug
                $this.css("max-width", dialog.options.maxWidth + "px");
            }
            //reposition dialog
            dialog.option("position", dialog.options.position);
        }
    });

}